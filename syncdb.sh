#!/bin/sh

. /etc/amue-authentic2/amue.conf

python /usr/lib/authentic2/manage.py syncdb --noinput
python /usr/lib/authentic2/manage.py migrate
