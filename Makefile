all:
	true

install:
	# stud
	install -d $(DESTDIR)/etc/stud/
	install -T -m 644 stud-amue-idp-test.conf $(DESTDIR)/etc/stud/idp.conf
	install -d $(DESTDIR)/etc/ssl/private/
	install -T -m 644 wildcard-amue.fr.cert $(DESTDIR)/etc/ssl/private/idp.pem
	# haproxy
	install -d $(DESTDIR)/etc/haproxy/
	install -T -m 644 haproxy.cfg $(DESTDIR)/etc/haproxy/haproxy-amue.cfg
	# custom template and static files
	install -d $(DESTDIR)/var/lib/authentic2/
	cp -R templates extra-static $(DESTDIR)/var/lib/authentic2/
	install -d $(DESTDIR)/etc/authentic2/
	cp config/nginx/idp.amue.fr $(DESTDIR)/etc/authentic2/idp.amue.fr.nginx
	cp config/authentic2/config.py $(DESTDIR)/etc/authentic2/
