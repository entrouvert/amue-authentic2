import os

REGISTRATION_OPEN=False
A2_CAN_RESET_PASSWORD=False
A2_HOMEPAGE_URL='http://www.amue.fr/'
CACHE_BACKEND={
    'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache', 
    'LOCATION': '127.0.0.1:11211'
    }
USE_X_FORWARDED_HOST=True
ALLOWED_HOSTS=['idptest.amue.fr', 'idp.amue.fr']
LDAP_AUTH_SETTINGS=[
        {
            "url": "ldap://194.167.237.11",
            "realm": "amue.fr",
            "basedn": "DC=wan,DC=amue,DC=fr",
            "binddn": "ldap.read@amue.fr",
            "bindpw": os.environ['AMUE_LDAP_PASSWORD'],
            "user_filter": "samaccountname=%s",
            "active_directory": True,
            "attributes": [
              "samaccountname",
              "mail",
              "sn",
              "cn",
              "givenName",
              "userprincipalname"],
            "email_field": "mail",
            "fname_field": "givenName",
            "lname_field": "sn",
            "username_template": "{samaccountname[0]}",
            "external_id_tuples": [["samaccountname:noquote"],["dn:noquote"]],
            "mandatory_attributes_values": {
                "edupersonaffiliation": ["employee"],
                "edupersonprimaryaffiliation": ["employee"],
                "edupersonscopedaffiliation": ["employee@amue.fr"],
            },
            "attribute_mappings": [
                ["userprincipalname", "edupersonprincipalname"],
            ],
            "transient": False,
        }
]
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')
A2_IDP_SAML2_ENABLE=True
